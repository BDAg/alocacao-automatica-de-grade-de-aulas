import csv

def criarVerticeProfessor(nome_professor, disciplina, disponibilidade):
    vertice = {
    "Professor": nome_professor,
    "Arestas": {
        "DaAulaDe": [disciplina],
        "DiasDisponiveis": disponibilidade
        }
    }
    professores.append(vertice)


def criarVerticeDisciplina(nome_disciplina, nome_professor, carga_horaria):
    vertice = {
    "Disciplina": nome_disciplina,
    "Arestas": {
        "ProfessoresDisponiveis": nome_professor,
        "NumeroDeAulas": carga_horaria,
        "AulasDisponiveis": int(carga_horaria)
        }
    }
    disciplina.append(vertice)


def criarVerticeDias(dia, quantidade_aulas, professores_disponiveis):
    vertice = {
    "Dia" : dia,
    "Arestas": {
        "QuantidadeAulas": quantidade_aulas,
        "ProfessoresDisponiveis": professores_disponiveis,
        "Aulas": []
        }
    }
    semana.append(vertice)

def _profDispoiveis(dia):
    prof = dict()
    try:
        for i in professores:
            if dia in i['Arestas']['DiasDisponiveis']:
                prof[i['Professor']] = len(i['Arestas']['DiasDisponiveis'])
    except:
        return False
    return sorted(prof, key=prof.get)

def _buscarVerticeProfessor(professor):
    for i in professores:
        if professor == i['Professor']:
            return i
    return None

def _buscarVerticeDisciplina(nome_disciplina):
    for i in disciplina:
        if nome_disciplina == i['Disciplina']:
            return i
    return None

def _buscarVerticeDia(dia):
    for i in semana:
        if dia == i['Dia']:
            return i
    return None

def _discDisponiveis(professor):
    profaux = _buscarVerticeProfessor(professor)
    for i in profaux['Arestas']['DaAulaDe']:
        dispaux = _buscarVerticeDisciplina(i)
        if dispaux['Arestas']['AulasDisponiveis'] != 0:
            return dispaux

professores = list()
disciplina = list()
semana = list()
aux_professor = set()
teste = open('teste.csv')
reader = csv.reader(teste)

for i in ['segunda', 'terca', 'quarta', 'quinta', 'sexta']:
    criarVerticeDias(i, 4, list())


for linha in reader:
    if linha[1] in aux_professor:
        _buscarVerticeProfessor(linha[1])['Arestas']['DaAulaDe'].append(linha[2])
        criarVerticeDisciplina(linha[2], linha[1], linha[3])
        continue
    criarVerticeProfessor(linha[1], linha[2], linha[4:])
    criarVerticeDisciplina(linha[2], linha[1], linha[3])
    for i in linha[4:]:
        _buscarVerticeDia(i)['Arestas']['ProfessoresDisponiveis'].append(linha[1])
    aux_professor.add(linha[1]) 

for dia in semana:
    profs = _profDispoiveis(dia['Dia'])
    for j in profs:
        if len(dia['Arestas']['Aulas']) >= 4:
            break
        j = _buscarVerticeProfessor(j)
        materia = _discDisponiveis(j['Professor'])
        if materia is None:
            continue
        for i in range(materia['Arestas']['AulasDisponiveis']):
            if len(dia['Arestas']['Aulas']) >= 4:
                break
            dia['Arestas']['Aulas'].append(materia['Disciplina'])
            materia['Arestas']['AulasDisponiveis'] = int(materia['Arestas']['AulasDisponiveis']) - 1

for i in semana:
    print(i)